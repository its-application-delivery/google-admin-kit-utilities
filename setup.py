from setuptools import setup

setup(name='umich_gak_utils',
      version='3.00',
      description='Utility functions for Google admin scripts',
      url='https://bitbucket.org/its-application-delivery/google-admin-kit-utilities/',
      author='Rob Carleski',
      author_email='carleski@umich.edu',
      license='MIT',
      packages=['umich_gak_utils'],
      zip_safe=False)
